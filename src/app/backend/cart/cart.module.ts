import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { Routes, RouterModule } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
// import { ModalModule, AlertModule, BsDatepickerModule, TypeaheadModule, BsDropdownModule, AccordionModule, AccordionComponent } from 'ngx-bootstrap';
import { CartComponent } from './cart.component'
import { GetlistComponent } from './getlist/getlist.component'

import { ModalModule } from 'ngx-bootstrap/modal'
import { AlertModule } from 'ngx-bootstrap/alert'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { TypeaheadModule } from 'ngx-bootstrap/typeahead'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown'
import { AccordionModule } from 'ngx-bootstrap/accordion'

const appRoutes: Routes = [
    {
        path: '',
        component: CartComponent,
        children: [
            { path: '', redirectTo: 'get-list' },
            { path: 'get-list', component: GetlistComponent }
        ]
    }
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(appRoutes),
        ModalModule.forRoot(),
        AlertModule.forRoot(),
        BsDropdownModule.forRoot(),
        TranslateModule,
        TypeaheadModule.forRoot(),
        BsDatepickerModule.forRoot(),
        AccordionModule.forRoot()
    ],
    // providers: [AccordionComponent],
    declarations: [CartComponent, GetlistComponent]
})
export class CartModule {}
