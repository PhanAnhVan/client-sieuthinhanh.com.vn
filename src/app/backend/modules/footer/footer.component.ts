import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../globals';

@Component({
  selector: 'admin-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(public globals: Globals,) { }

  ngOnInit() {
  }

}
