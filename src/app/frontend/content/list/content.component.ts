import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Globals } from '../../../globals';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.css'],

})
export class ListContentComponent implements OnInit, OnDestroy {
    public cstbcontent = new TableService();
    public connect;
    public link;
    public width;
    public data: any = {};
    public show: number = -1;
    public token: any = {
        getContent: "api/getContent",
    }

    public cwstable = new TableService();

    constructor(
        public globals: Globals,
        public route: ActivatedRoute,
        public router: Router
    ) {

        this.width = document.body.getBoundingClientRect().width;
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getContent":
                    this.data = [];
                    this.data = res.data;
                    this.show = this.data.list && this.data.list.length > 0 ? 1 : 0;

                    this.cwstable._concat(this.data.list, true);
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.cwstable._ini({ data: [], keyword: 'getContent', count: this.Pagination.itemsPerPage, sorting: { field: "maker_date", sort: "DESC", type: "date" } });

        this.route.params.subscribe(params => {
            this.link = params.link;
            if (this.link && this.link.toString().length > 0) {
                this.globals.send({ path: this.token.getContent, token: "getContent", params: { link: this.link } });
            } else {
                this.globals.send({ path: this.token.getContent, token: "getContent" });
            }
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    public Pagination = {

        maxSize: 5,

        itemsPerPage: 12,


        change: (event: PageChangedEvent) => {

            const startItem = (event.page - 1) * event.itemsPerPage;

            const endItem = event.page * event.itemsPerPage;

            this.cwstable.data = this.cwstable.cached.slice(startItem, endItem);

            var el = document.getElementById('ListData');

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    }
}