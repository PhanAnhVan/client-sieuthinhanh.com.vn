import { Component, OnInit } from '@angular/core';
import { Globals } from '../../globals';
import { Router, ActivatedRoute, } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

    public connect;
    public id: any;
    public link: string;


    constructor(

        public fb: FormBuilder,
        public globals: Globals,
        private routerAct: ActivatedRoute,
        public translate: TranslateService,
        public router: Router,
        private toastr: ToastrService,

    ) {


    }

    ngOnInit() {
        let data = this.globals.CUSTOMER.get(true);
        this.id = data.id
        this.routerAct.url.subscribe(url => {
            setTimeout(() => {
                this.link = url[0]['path']
            }, 250);

        })
    }
}
