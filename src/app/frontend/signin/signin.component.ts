import { Component, OnInit, OnDestroy } from '@angular/core';
import { Globals } from '../../globals';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit, OnDestroy {

    public connect;

    public flags: boolean = true;

    public width: number;

    public data: any = {};

    public token: any = {

        getPageLogin: "api/getPageByLink",

    }
    constructor(

        public globals: Globals,
        public route: ActivatedRoute,

    ) {
        this.width = document.body.getBoundingClientRect().width;

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getPageLogin":
                    this.data = res.data;
                    break;
                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getPageLogin, token: "getPageLogin", params: { link: 'dang-nhap' } });
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe();
        }
    }

}