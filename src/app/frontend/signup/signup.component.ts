import { Component, OnInit, OnDestroy } from '@angular/core';
import { Globals } from '../../globals';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {


    public connect;

    public flags: boolean = true;

    public width: number;

    public data: any = {};

    public token: any = {
        getPageRegister: "api/getPageByLink",
    }
    constructor(
        public globals: Globals,
        public route: ActivatedRoute,

    ) {
        this.width = document.body.getBoundingClientRect().width;

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getPageRegister":
                    this.data = res.data
                    break;
                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getPageRegister, token: "getPageRegister", params: { link: 'dang-ky' } });
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe();
        }
    }
}