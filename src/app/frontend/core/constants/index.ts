// Images config
export const LOGO = '../../../../assets/img/demo-logo.png'
export const LOADING_CIRCLE = '../../../../assets/img/loading.gif'
export const ICON_ZALO = '../../../../assets/img/icon-zalo.png'
export const SLIDE_ARROW_LEFT = '../../../../assets/img/left-chevron.png'
export const SLIDE_ARROW_RIGHT = '../../../../assets/img/right-arrow.png'
